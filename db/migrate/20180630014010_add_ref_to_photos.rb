class AddRefToPhotos < ActiveRecord::Migration[5.2]
  def change
    add_reference :photos, :place, foreign_key: true
  end
end
